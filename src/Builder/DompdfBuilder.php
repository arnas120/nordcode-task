<?php

declare(strict_types=1);

namespace App\Builder;

use Dompdf\Dompdf;

class DompdfBuilder
{
    private const PAPER_SIZE = 'A4';
    private const PAPER_ORIENTATION = 'portrait';

    /**
     * @param string $html
     * @return Dompdf
     */
    public function build(string $html): Dompdf
    {
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper(self::PAPER_SIZE, self::PAPER_ORIENTATION);

        return $dompdf;
    }
}
