<?php

declare(strict_types=1);

namespace App\Builder;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Model\Spreadsheet as SpreadsheetModel;

class SpreadsheetBuilder
{
    /**
     * @param SpreadsheetModel $spreadsheetModel
     * @return Spreadsheet
     */
    public function build(SpreadsheetModel $spreadsheetModel): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $this->buildColumns($spreadsheetModel->getColumns(), $sheet);
        $this->buildColumnValues($spreadsheetModel->getColumnValues(), $sheet);

        return $spreadsheet;
    }

    /**
     * @param array $columns
     * @param Worksheet $worksheet
     */
    private function buildColumns(array $columns, Worksheet $worksheet): void
    {
        $columnLetter = 'A';
        foreach ($columns as $columnName) {
            $columnLetter++;
            $worksheet->setCellValue($columnLetter . '2', $columnName);
        }
    }

    /**
     * @param array $columnValues
     * @param Worksheet $worksheet
     */
    private function buildColumnValues(array $columnValues, Worksheet $worksheet): void
    {
        $i = 3;
        foreach ($columnValues as $columnValue) {
            $columnLetter = 'A';
            foreach ($columnValue as $value) {
                $columnLetter++;
                $worksheet->setCellValue($columnLetter . $i, $value);
            }
            $i++;
        }
    }
}
