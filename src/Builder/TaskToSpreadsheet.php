<?php

declare(strict_types=1);

namespace App\Builder;

use App\Model\Spreadsheet;
use App\Entity\Task;

class TaskToSpreadsheet
{
    private const TASK_COLUMNS = [
        'Task name',
        'Comment',
        'Date',
        'Amount of time',
    ];

    /**
     * @param Task[] $tasks
     * @return Spreadsheet
     */
    public function build(array $tasks): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setColumns(self::TASK_COLUMNS);
        $spreadsheet->setColumnValues($this->buildTaskColumnValues($tasks));

        return $spreadsheet;
    }

    /**
     * @param Task[] $tasks
     * @return array
     */
    private function buildTaskColumnValues(array $tasks): array
    {
        $columnValues = [];
        foreach ($tasks as $task) {
            $columnValues[] = [
                $task->getTitle(),
                $task->getComment(),
                $task->getDate(),
                $task->getTime(),
            ];
        }

        return $columnValues;
    }
}
