<?php

namespace App\Controller;

use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\TaskRepository;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Service\TaskService;
use App\Builder\DompdfBuilder;
use App\Service\PdfGenerator\PdfGenerator;
use App\Entity\DocumentType;
use App\Service\SpreadsheetGenerator\SpreadsheetGenerator;
use App\Builder\TaskToSpreadsheet;

/**
 * @Route("/api",name="api_")
 */
class TaskApiController extends AbstractFOSRestController
{
    /**
     * @var TaskService
     */
    private TaskService $taskService;

    /**
     * @var TaskRepository
     */
    private TaskRepository $taskRepository;

    /**
     * @var PdfGenerator
     */
    private PdfGenerator $pdfGenerator;

    /**
     * @var SpreadsheetGenerator
     */
    private SpreadsheetGenerator $spreadsheetGenerator;

    /**
     * @var TaskToSpreadsheet
     */
    private TaskToSpreadsheet $tasksToSpreadsheetBuilder;

    /**
     * TaskApiController constructor.
     * @param TaskService $taskService
     * @param TaskRepository $taskRepository
     * @param PdfGenerator $pdfGenerator
     * @param SpreadsheetGenerator $spreadsheetGenerator
     * @param TaskToSpreadsheet $tasksToSpreadsheetBuilder
     */
    public function __construct(
        TaskService $taskService,
        TaskRepository $taskRepository,
        PdfGenerator $pdfGenerator,
        SpreadsheetGenerator $spreadsheetGenerator,
        TaskToSpreadsheet $tasksToSpreadsheetBuilder
    ) {
        $this->taskService = $taskService;
        $this->taskRepository = $taskRepository;
        $this->pdfGenerator = $pdfGenerator;
        $this->spreadsheetGenerator = $spreadsheetGenerator;
        $this->tasksToSpreadsheetBuilder = $tasksToSpreadsheetBuilder;
    }

    /**
     * @Rest\Post("/create-task")
     *
     * @param Request $request
     * @return Response
     */
    public function createTask(Request $request): Response
    {
        $this->taskService->createTask($request);

        return new Response();
    }

    /**
     * @Rest\Get("/export-tasks/user/{userId}/type/{type}")
     *
     * @param int $userId
     * @param string $type
     * @param Request $request
     * @throws Exception
     */
    public function exportTasks(int $userId, string $type, Request $request)
    {
        $userTasks = $this->taskRepository->findByUserAndDateRange(
            $userId,
            $request->get('dateFrom'),
            $request->get('dateTo')
        );

        switch ($type) {
            case 'pdf':
                return $this->pdfGenerator->generate($userTasks, DocumentType::DOCUMENT_TYPE_REPORT);
            case 'xlsx':
            case 'csv':
                $spreadSheet = $this->tasksToSpreadsheetBuilder->build($userTasks);

                return $this->spreadsheetGenerator->generate($spreadSheet, $type);
        }

        throw new Exception('Type is not supported');
    }
}
