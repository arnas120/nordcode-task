<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\TaskRepository;

class TasksController extends AbstractController
{
    /**
     * @var TaskRepository
     */
    private TaskRepository $taskRepository;

    /**
     * @var PaginatorInterface
     */
    private PaginatorInterface $paginator;

    /**
     * TasksController constructor.
     * @param TaskRepository $taskRepository
     * @param PaginatorInterface $paginator
     */
    public function __construct(TaskRepository $taskRepository, PaginatorInterface $paginator)
    {
        $this->taskRepository = $taskRepository;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/tasks", name="app_tasks")
     * @param Request $request
     * @return Response
     */
    public function tasks(Request $request): Response
    {
        if (!$user = $this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        $userTasks = $this->taskRepository->findByUserLatest($user->getId());
        $pagination = $this->paginator->paginate($userTasks, $request->query->getInt('page', 1), 10);

        return $this->render('task/task.html.twig', ['paginatedTasks' => $pagination]);
    }
}
