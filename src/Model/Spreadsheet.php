<?php

declare(strict_types=1);

namespace App\Model;

class Spreadsheet
{
    /**
     * @var array
     */
    private array $columns;

    /**
     * @var array
     */
    private array $columnValues;

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns)
    {
        $this->columns = $columns;
    }

    /**
     * @return array
     */
    public function getColumnValues(): array
    {
        return $this->columnValues;
    }

    /**
     * @param array $columnValues
     */
    public function setColumnValues(array $columnValues): void
    {
        $this->columnValues = $columnValues;
    }
}
