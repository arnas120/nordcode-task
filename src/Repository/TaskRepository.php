<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    /**
     * TaskRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    /**
     * @param int $userId
     * @return array
     */
    public function findByUserLatest(int $userId): array
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.user = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('u.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $userId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function findByUserAndDateRange(int $userId, string $dateFrom, string $dateTo): array
    {
        return $this->createQueryBuilder('u')
            ->where('u.date BETWEEN :dateFrom AND :dateTo')
            ->andWhere('u.user = :userId')
            ->setParameter('dateFrom', $dateFrom)
            ->setParameter('dateTo', $dateTo)
            ->setParameter('userId', $userId)
            ->orderBy('u.date', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
