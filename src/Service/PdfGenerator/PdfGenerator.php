<?php

declare(strict_types=1);

namespace App\Service\PdfGenerator;

use App\Builder\DompdfBuilder;
use Twig\Environment;
use App\Repository\DocumentTypeRepository;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class PdfGenerator
{
    /**
     * @var Environment
     */
    private Environment $twig;

    /**
     * @var DompdfBuilder
     */
    private DompdfBuilder $pdfBuilder;

    /**
     * @var DocumentTypeRepository
     */
    private DocumentTypeRepository $documentTypeRepository;

    /**
     * PdfGenerator constructor.
     * @param Environment $twig
     * @param DompdfBuilder $pdfBuilder
     * @param DocumentTypeRepository $documentTypeRepository
     */
    public function __construct(
        Environment $twig,
        DompdfBuilder $pdfBuilder,
        DocumentTypeRepository $documentTypeRepository
    ) {
        $this->twig = $twig;
        $this->pdfBuilder = $pdfBuilder;
        $this->documentTypeRepository = $documentTypeRepository;
    }

    /**
     * @param array $data
     * @param string $documentType
     */
    public function generate(array $data, string $documentType): void
    {
        $dompdf = $this->pdfBuilder->build($this->getDocumentData($data, $documentType));
        $dompdf->render();
        ob_end_clean();
        $dompdf->stream(uniqid() . ".pdf", ["Attachment" => true]);
    }

    /**
     * @param array $data
     * @param string $documentType
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function getDocumentData(array $data, string $documentType): string
    {
        return $this->twig->render($this->getDocumentTemplate($documentType), [
            'data' => $data,
        ]);
    }

    /**
     * @param string $documentType
     * @return string
     */
    private function getDocumentTemplate(string $documentType): string
    {
        $documentType = $this->documentTypeRepository->findOneBy(['name' => $documentType]);

        return 'pdf/' . $documentType->getTemplate() . '.html.twig';
    }
}
