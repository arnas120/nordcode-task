<?php

declare(strict_types=1);

namespace App\Service\SpreadsheetGenerator;

use App\Builder\SpreadsheetBuilder;
use App\Model\Spreadsheet;
use Exception;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;

class SpreadsheetGenerator
{
    public const SPREADSHEET_TYPE_CSV = 'csv';
    public const SPREADSHEET_TYPE_XLSX = 'xlsx';
    private const CONTENT_TYPE_XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    private const CONTENT_TYPE_CSV = 'text/csv';

    /**
     * @var SpreadsheetBuilder
     */
    private SpreadsheetBuilder $spreadSheetBuilder;

    public function __construct(SpreadsheetBuilder $spreadSheetBuilder)
    {
        $this->spreadSheetBuilder = $spreadSheetBuilder;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param string $type
     * @return StreamedResponse
     * @throws Exception
     */
    public function generate(Spreadsheet $spreadsheet, string $type): StreamedResponse
    {
        $spreadSheetObject = $this->spreadSheetBuilder->build($spreadsheet);

        switch ($type) {
            case self::SPREADSHEET_TYPE_CSV:
                $contentType = self::CONTENT_TYPE_CSV;
                $writer = new Csv($spreadSheetObject);
                break;
            case self::SPREADSHEET_TYPE_XLSX:
                $contentType = self::CONTENT_TYPE_XLSX;
                $writer = new Xlsx($spreadSheetObject);
                break;
            default:
                throw new Exception('Spreadsheet type not supported');
        }

        return $this->buildStreamedResponse($writer, $contentType, $type);
    }

    /**
     * @param BaseWriter $writer
     * @param string $contentType
     * @param string $type
     * @return StreamedResponse
     */
    private function buildStreamedResponse(BaseWriter $writer, string $contentType, string $type): StreamedResponse
    {
        $response = new StreamedResponse();
        $response->headers->set('Content-Type', $contentType);
        $response->headers->set(
            'Content-Disposition', 'attachment;filename="' . uniqid() . '.' . $type . '"'
        );
        $response->setPrivate();
        $response->headers->addCacheControlDirective('no-cache', true);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->setCallback(function () use ($writer) {
            $writer->save('php://output');
        });

        return $response;
    }
}
