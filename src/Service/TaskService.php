<?php

declare(strict_types=1);

namespace App\Service;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Task;

class TaskService
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * TaskService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @throws Exception
     */
    public function createTask(Request $request): void
    {
        $time = (int)$request->get('time');

        if ($time < 0) {
            throw new Exception('Time must be grater than zero', 400);
        }

        $task = new Task();
        $task->setUser($request->get('userId'));
        $task->setTitle($request->get('title'));
        $task->setComment($request->get('comment'));
        $task->setDate(new DateTime($request->get('date')));
        $task->setTime($time);

        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }
}
